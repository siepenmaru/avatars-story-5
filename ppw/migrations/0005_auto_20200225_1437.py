# Generated by Django 3.0.3 on 2020-02-25 07:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ppw', '0004_classyear_class_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='friend',
            name='favourite_food_drink',
            field=models.CharField(max_length=30),
        ),
    ]
