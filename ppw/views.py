from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect

from .models import Friend, ClassYear
from .forms import FriendForm

# Create your views here.
def index(request):
    return render (request, "index.html")
def about(request):
    return render (request, "about-me.html")
def work(request):
    return render (request, "my-work.html")
def contact(request):
    return render (request, "contact-me.html")
def friends(request):
    friends = Friend.objects.all()
    response = {"name": "My_Name", "Friends": friends}
    return render(request, "friends.html", {"friends":friends})
def new_friend(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = FriendForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            form.save()
            friends = Friend.objects.all()
            # redirect to a new URL:
            return render(request, "friends.html", {"friends":friends})

    # if a GET (or any other method) we'll create a blank form
    else:
        form = FriendForm()

    return render(request, 'new-friend.html', {'form': form})