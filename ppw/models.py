from django.db import models
from django.utils import timezone
from datetime import datetime, date

class ClassYear(models.Model):
    class_year = models.IntegerField()
    class_name = models.CharField(max_length=30)
    def __str__(self):
        return (self.class_name + " " + str(self.class_year))

class Friend(models.Model):
    name = models.CharField(max_length=30)
    hobby = models.CharField(max_length=30)
    favourite_food_drink = models.CharField(max_length=30)
    class_year = models.ForeignKey(ClassYear, on_delete= models.CASCADE)
    date_posted = models.DateTimeField(auto_now_add= True)
    date_updated = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.name