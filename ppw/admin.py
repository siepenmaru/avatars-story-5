from django.contrib import admin
from .models import Friend, ClassYear

admin.site.register(Friend)
admin.site.register(ClassYear)