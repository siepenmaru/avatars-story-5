from django.forms import ModelForm
from .models import Friend

class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = ["name", "hobby", "favourite_food_drink", "class_year"]

# class FriendForm(forms.Form):
#     form_name = forms.CharField(label='Your name', max_length=30, required=True)
#     form_hobby = forms.CharField(label='Your hobby', max_length=30, required=False)
#     form_fav = forms.CharField(label='Your favourite food/drink', max_length=30, required=False)
#     form_year = forms.ChoiceField(choices={x for x in ClassYear.objects.all()}, required=True)
    